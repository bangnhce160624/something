package su22_01_ngohaibang_ce160624;

import java.util.Scanner;

/**
 * Lab_01 SE1606
 *
 * @author Ngo Hai Bang CE160624
 */
public class FileProcessing {

    private static Manager Mana;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        boolean valid = true;
        do {    //This loop so that the program does not exit when the user enters a letter but forces the user to choose again

            try {
                valid = true;
                Mana = new Manager();   //Create object

                Scanner cin = new Scanner(System.in);   //Creates new a scanner
                int func;   //The function that selected by user

                do { //this loop to print menu and excute it until the user selects 6
                    System.out.println("============ File Processing ============");
                    System.out.println("1. Check Path");
                    System.out.println("2. Get file name with type java");
                    System.out.println("3. Get file with size greater than input");
                    System.out.println("4. Write more content to file");
                    System.out.println("5. Read file and count characters");
                    System.out.println("6. Exit");
                    System.out.print("Please choice one optine: ");
                    func = cin.nextInt();
                    cin.nextLine();

                    switch (func) {
                        case 1:
                            System.out.println("------------- Check Path ------------");
                            String path;

                            // Gets the path
                            do {    //this loop to force the user to re-enter when the user doesn't enter anything
                                System.out.print("Enter Path:");
                                path = cin.nextLine().trim();
                                if (path.equals("")) {  // check if it's empty
                                    System.out.println("Error: path can not be empty!");
                                }
                            } while (path.equals(""));
                            Mana.setPath(path); //Set the path for the ojb
                            Mana.checkInputPath();  //checks if the input path exists, checks if it is a directory or a file

                            break;
                        case 2:
                            System.out.println("----- Get file name with type java -----");
                            String path2;

                            // Gets the path
                            do {    //this loop to force the user to re-enter when the user doesn't enter anything
                                System.out.print("Enter Path:");
                                path2 = cin.nextLine().trim();
                                if (path2.equals("")) {  // check if it's empty
                                    System.out.println("Error: path can not be empty!");
                                }
                            } while (path2.equals(""));
                            Mana.setPath(path2); //Set the path for the obj
                            Mana.getAllFileNameJavaInDirectory();  //get all java file names in directory and print them

                            break;
                        case 3:
                            System.out.println("--- Get file with size greater than input ---");
                            int inputSize = 0;
                            boolean isValid = true;
                            String path3;

                            // Gets the size
                            do {    ////this loop to force the user to re-enter when the user enter wrong data
                                try {
                                    isValid = true;
                                    System.out.print("Enter Size: ");
                                    inputSize = cin.nextInt();
                                    cin.nextLine();

                                    if (inputSize <= 0) {   //check if it's less than or equal 0
                                        System.out.println("Error: Size must be greater than 0!");
                                    }
                                } catch (Exception e) {
                                    // Error if user input wrong data
                                    System.out.println("Size is digit");
                                    isValid = false;
                                    cin.nextLine();
                                }
                            } while (inputSize <= 0 || isValid == false);
                            Mana.setSize(inputSize); //Set the input size for the obj

                            // Gets the path
                            do {    //this loop to force the user to re-enter when the user doesn't enter anything
                                System.out.print("Enter Path:");
                                path3 = cin.nextLine().trim();
                                if (path3.equals("")) {  //check if it's empty
                                    System.out.println("Error: path can not be empty!");
                                }
                            } while (path3.equals(""));
                            Mana.setPath(path3);    // Set the path for the obj
                            Mana.getFileWithSizeGreaterThanInput(); //get file with size greater than input size and print them

                            break;
                        case 4:
                            System.out.println("----- Write more content to file -----");
                            String path4;
                            String contentInput;

                            // Gets the content
                            do {    //this loop to force the user to re-enter when the user doesn't enter anything
                                System.out.print("Enter Content: ");
                                contentInput = cin.nextLine().trim();
                                if (contentInput.equals("")) {  // check if it's empty
                                    System.out.println("Error: path can not be empty!");
                                }
                            } while (contentInput.equals(""));
                            Mana.setContent(contentInput);  //Set the content for the obj

                            // Gets the path
                            do {    //this loop to force the user to re-enter when the user doesn't enter anything
                                System.out.print("Enter Path: ");
                                path4 = cin.nextLine().trim();
                                if (path4.equals("")) {  // check if it's empty
                                    System.out.println("Error: path can not be empty!");
                                }
                            } while (path4.equals(""));
                            Mana.setPath(path4);    //Set the path for the obj

                            if (Mana.appendContentToFile()) {    //append the content to the file
                                System.out.println("Write done");
                            } else {
                                System.out.println("Exception occurred:");
                            }

                            break;
                        case 5:
                            System.out.println("----- Read file an count characters -----");
                            String path5;

                            // Gets the path
                            do {    //this loop to force the user to re-enter when the user doesn't enter anything
                                System.out.print("Enter Path: ");
                                path5 = cin.nextLine().trim();
                                if (path5.equals("")) {  // check if it's empty
                                    System.out.println("Error: path can not be empty!");
                                }
                            } while (path5.equals(""));
                            Mana.setPath(path5);    //Set the path for the obj
                            int count = Mana.countCharacter(); //count character in the file
                            if (count != -1) {
                                System.out.println("Total: " + count);
                            }

                            break;
                        case 6:
                            System.out.println("==========================");
                            System.out.println("Thank you for using our software!\n");
                            break;
                        default:
                            System.out.println("Error: The selection must be a integer number from 1 to 6!");
                    }

                } while (func != 6); // loop exit if the user selects 6
            } catch (Exception e) {
                // Error if user input wrong data
                System.out.println("Error: The selection must be a integer number from 1 to 6!");
                valid = false;
            }
        } while (valid == false);
    }

}
