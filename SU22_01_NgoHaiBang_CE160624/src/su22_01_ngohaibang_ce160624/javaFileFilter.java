package su22_01_ngohaibang_ce160624;

import java.io.File;
import java.io.FileFilter;

/**
 * Lab_01 SE1606
 *
 * @author Ngo Hai Bang CE160624
 */
public class javaFileFilter implements FileFilter {

    // Only accepts 'pathname' as file and has 'extension' as java.
    @Override
    public boolean accept(File pathname) {

        if (!pathname.isFile()) {   //Check if it's not a file
            return false;
        }

        if (pathname.getAbsolutePath().endsWith(".java")) { //Check if it's java file
            return true;
        }

        return false;
    }
}
