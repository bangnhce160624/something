package su22_01_ngohaibang_ce160624;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Lab_01 SE1606
 *
 * @author Ngo Hai Bang CE160624
 */
public class Manager {

    private String path;
    private String content;
    private int size;

    /**
     *
     * Create constructor
     */
    public Manager() {

    }

    /**
     * Create constructor
     *
     * @param path
     * @param content
     * @param size
     */
    public Manager(String path, String content, int size) {
        this.path = path;
        this.content = content;
        this.size = size;
    }

    /**
     * Get the path of Manager
     *
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     * Set path for Manager
     *
     * @param path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Get the content of Manager
     *
     * @return
     */
    public String getContent() {
        return content;
    }

    /**
     * Set content for Manager
     *
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Get the size of Manager
     *
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     * Set size for Manager
     *
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    Scanner cin = new Scanner(System.in); // Creates scanner

    /**
     * This method checks if the input path exists, checks if it is a directory
     * or a file
     *
     */
    public void checkInputPath() {
        File f = new File(this.path);
        if (!f.exists()) {                              //check if it's not exist
            System.out.println("Path do not exist!");
        } else if (f.isDirectory()) {                   //check if it's Directory
            System.out.println("Path to Directory");
        } else if (f.isFile()) {                        //check if it's file
            System.out.println("Path to file");
        }
    }

    /**
     * Get all java file names in directory
     *
     */
    public void getAllFileNameJavaInDirectory() {
        File f = new File(this.path);

        if (!f.exists()) {  //check if it's not exist
            System.out.println("Path do not exist!");
        } else {
            File[] javaFiles = f.listFiles(new javaFileFilter());   //check if it's a java file?

            for (File javaFile : javaFiles) {   //this loop will print all absolute path of java file
                System.out.println(javaFile.getAbsolutePath());
            }

            System.out.println("Result " + javaFiles.length + " file!");    //print number of java file name in the directory
        }
    }

    /**
     * Get file with size greater than input size
     *
     */
    public void getFileWithSizeGreaterThanInput() {
        File f = new File(this.path);
        int count = 0;

        if (!f.exists()) {  //check if it's not exist
            System.out.println("Path do not exist!");
        } else {
            File[] allFiles = f.listFiles();    //save all files and folders in array

            for (File file : allFiles) {
                if (file.length() / 1024 > this.size && file.isFile()) { //check if the file is larger than the input size
                    System.out.println(file.getAbsolutePath());     //divide by 1024 because the problem is kilobytes
                    ++count;
                }
            }
            System.out.println("Result " + count + " file!");
        }
    }

    /**
     * Write more content to the file
     *
     * @return
     * @throws java.io.IOException
     */
    public boolean appendContentToFile() throws IOException {
        File f = new File(this.path);

        if (!f.exists()) { //check if it's not exist
            System.out.println("Path do not exist!");
            return false;
        } else {
            FileWriter fw = new FileWriter(f, true);
            fw.append(" ");     //This will add a space to the file content
            fw.append(this.content);   //This will add the content to the file
            fw.close();
        }
        return true;
    }

    /**
     * Count character
     *
     * @return
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    public int countCharacter() throws FileNotFoundException, IOException {
        int count = 0;
        File f = new File(this.path);

        if (!f.exists()) {  //check if it's not exist
            System.out.println("Path do not exist!");
            return -1;
        } else if (!f.isFile() || (!this.path.endsWith(".txt"))) {    //check if it's not a txt file
            System.out.println("Path must be a txt file!");
            return -1;
        } else {
            BufferedReader br = new BufferedReader(new FileReader(f));
            try {
                String str = br.readLine(); //read content from the file
                String[] array = str.split("[ ,.]");    //split the string into an array that stores the words separated by spaces
                for (String string : array) {   //count word in the array
                    if (!string.equals("")) {
                        ++count;
                    }
                }
            } finally {
                br.close();
            }
        }
        return count;
    }
}
